//
//  ViewController.swift
//  20160512_iOSDay11_ServiceAPI
//
//  Created by ChenSean on 5/12/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NSURLSessionDataDelegate {
    @IBOutlet weak var txt_X: UITextField!
    @IBOutlet weak var txt_Y: UITextField!
    @IBAction func btn_Submit(sender: AnyObject) {
        self.Sumbmit(txt_X.text!, y: txt_Y.text!)
    }
    @IBOutlet weak var lbl_Result: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func Sumbmit(x:String, y:String){
        
        let url = NSURL(string: "http://210.60.95.1/~ckk/b.php")
        
        let request = NSMutableURLRequest(URL: url!, cachePolicy: .ReloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        request.HTTPBody = String(format: "x=%@&y=%@",self.txt_X.text! ,self.txt_Y.text!).dataUsingEncoding(NSUTF8StringEncoding)
        request.HTTPMethod = "POST"
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: nil) // delegateQueue 如果設定 NSOperationQueue.mainQueue() 會做在主執行緒，很少會這樣用
        let dataTask = session.dataTaskWithRequest(request)
        
        dataTask.resume()
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        // 抓到佇列的進入點
        let q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        // asyn 是將佇列中的執行緒拿出來
        dispatch_async(q) {
            if let html = NSString(data: data, encoding: NSUTF8StringEncoding) {
                dispatch_async(dispatch_get_main_queue(), {
                    self.lbl_Result.text = html as String;
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

